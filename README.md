# Pure Shadow
a spa of personal blog

# WIP

## How to use

1. make sure your html including `index.min.js` and having an div which id is `pureshadow`.
2. Adding the text of `config.yaml` to text content of your `pureshadow` div.
3. editing `nav` to include your articals.
4. deploy as in this project host public as your root.

## How to build
(`closure-compiler`, `uglifyjs`, `stack` are needed)
1. `npm -g install purescript pulp bower`
2. `bower install`
3. `npm install`
4. `npm run build`