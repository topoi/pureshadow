module Main where

import Prelude
import Effect (Effect)
import Effect.Class (liftEffect)

import Affjax as AX
import Affjax.ResponseFormat as ResponseFormat
import Data.Either (Either(..))
import Data.Maybe (Maybe(..))
-- import Data.HTTP.Method (Method(..))
import Effect.Aff (launchAff, launchAff_)
import Effect.Class.Console (log)
import Effect.Exception (throw)
import Web.HTML (window)
import Web.HTML.HTMLDocument (toNonElementParentNode)
import Web.HTML.Window (document)
import Web.DOM.NonElementParentNode (getElementById)

import Web.DOM.Element(toNode)
import Web.DOM.Node(nodeName, textContent)

import Config

import Concur.React.Run (runWidgetInDom)
import Articals as A

displayId :: String
displayId = "pureshadow"

main :: Effect Unit
main = do
  cfg <- getConfig
  runWidgetInDom displayId $ A.routingWidget cfg

getConfig :: Effect BlogConfig
getConfig = do
  documentType <- document =<< window
  element <- getElementById displayId $ toNonElementParentNode documentType
  configStr <- case element of
      Nothing -> throw "app not found"
      Just e -> textContent $ toNode e
  case loadConfig configStr of
    Left err -> throw err
    Right cfg -> pure cfg