module Config where

import Data.Argonaut.Decode
import Data.Maybe
import Prelude

import Control.Monad.Except (runExcept)
import Data.Either (Either(..))
import Data.Tuple (Tuple)
import Data.YAML.Foreign.Decode (parseYAMLToJson)
import Foreign.Object as F

configUrl :: String
configUrl = "/config.yaml"

newtype BlogConfig = BlogConfig
  { siteName :: String
  , articals :: Array (Tuple String String)
  }

instance decodeJsonBlogConfig :: DecodeJson BlogConfig where
  decodeJson json = do
    x <- decodeJson json
    name <- x .: "site_name"
    as <- x .: "nav" >>= decodeJson
    pure $ BlogConfig {siteName: name, articals: F.toUnfoldable as}

instance showBlogConfig :: Show BlogConfig where
  show (BlogConfig c) = show c.siteName <> show c.articals

loadConfig :: String -> Either String BlogConfig
loadConfig c = case runExcept $ parseYAMLToJson c of
  Left err -> Left "Could not parse yaml"
  Right json -> decodeJson json

newtype Essay = Essay
  { what :: String
  , how :: String
  , why :: String
  , app :: String
  }

instance decodeJsonEssay :: DecodeJson Essay where
  decodeJson json = do
    x <- decodeJson json
    w <- x .: "what"
    h <- x .: "how"
    y <- x .: "why"
    a <- x .: "app"
    pure $ Essay {what: w, how: h, why: y, app: a}