module Articals where

import Prelude

import Concur.Core (Widget)
import Concur.React (HTML)
import Concur.React.DOM as D
import Concur.React.Props as P
import Control.Alt ((<|>))
import Data.Foldable (oneOf)
import Effect.AVar as Evar
import Effect.Aff 
import Effect.Aff.AVar as Avar
import Effect.Aff.Class (liftAff)
import Effect.Class (liftEffect)
import Routing.Hash (matches)
import Routing.Match (Match, end, str, root)

import Affjax as AX
import Affjax.ResponseFormat as ResponseFormat

import Data.Either (Either(..))
import Data.Tuple
import Data.Maybe

-- import Data.String as S

import Config

-- To route, we start listening for route changes with `matches`
-- On each route change we push the route to a var
-- Then we listen on the var asynchronously from within the UI with `awaitRoute`
routingWidget :: forall a. BlogConfig -> Widget HTML a
routingWidget blogConfig = do
  routeRef <- liftEffect $ do
    var <- Evar.empty
    void $ matches myRoutes \_ route -> void $ Evar.tryPut route var
    pure var
  let awaitRoute = liftAff $ Avar.take routeRef
  -- HACK: This delay is only needed the first time
  -- Since the page might still be loading,
  -- and there are weird interactions between loading the homepage and the current route
  liftAff (delay (Milliseconds 0.0))
  go awaitRoute Home
  where
  go awaitRoute route = do
    route' <- awaitRoute <|> pageForRoute blogConfig route
    go awaitRoute route'

-- Route and associated pages
data MyRoute
  = Home
  | Page String

myRoutes :: Match MyRoute
myRoutes = root *> oneOf
  [ Home <$ end
  , Page <$> str <* end
  ]

pageForRoute :: forall a. BlogConfig -> MyRoute -> Widget HTML a
pageForRoute (BlogConfig config) Home = homePage where
  homePage :: forall a1. Widget HTML a1
  homePage = D.div'
    [ D.h1' [D.text config.siteName]
    , D.div' $ (\(Tuple t ct) -> D.div' [D.a [P.href $ "#/" <> t] [D.text $ "Blog: " <> t]]) <$> config.articals
    ]

pageForRoute (BlogConfig config) (Page s) = page where
  page :: forall a2. Widget HTML a2
  page = do
    ct <- liftAff $ do
      case lookup s config.articals of
        Nothing -> pure "page not find"
        Just u -> url2Content u
    D.div'
      [ D.h1' [D.text $ "You are on Page " <> s]
      , textDisplay ct
      , D.div' [D.a [P.href "#/"] [D.text "Go Home"]]
      ]

url2Content :: String -> Aff String
url2Content url = do
  cc <- AX.get ResponseFormat.string url
  let content = case cc.body of
        Left err -> Left $ "GET " <> url <> " response failed to decode: " <> AX.printResponseFormatError err
        Right ss -> pure ss
  case content of
    Left err -> pure err
    Right ct -> pure ct

textDisplay :: forall a. String -> Widget HTML a
textDisplay s = D.pre' $ [D.text s]

essayDisplay :: forall a. Essay -> Widget HTML a
essayDisplay (Essay es) = do
  D.div'
    [ D.h3' [D.text $ "what"]
    , D.text es.what
    , D.h3' [D.text $ "how"]
    , D.text es.how
    , D.h3' [D.text $ "why"]
    , D.text es.why
    , D.h3' [D.text $ "app"]
    , D.text es.app
    ]